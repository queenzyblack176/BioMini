package com.queen.biomini;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.queen.biomini.classes.CheckIn;
import com.queen.biomini.classes.Client;

import java.util.List;

public class ClientViewModel extends AndroidViewModel {
//    private ClientRepository repository;
    private  LiveData<List<Client>> clients;
    private  LiveData<List<CheckIn>> checkIns;
    public ClientViewModel(@NonNull Application application) {
        super(application);
//        respository = new ClientRepository(application);
//        clients = repository.getClients();
    }

    public LiveData<List<Client>> getClients() {
        return clients;
    }
}
