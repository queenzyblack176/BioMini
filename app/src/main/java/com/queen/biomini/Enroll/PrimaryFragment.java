package com.queen.biomini.Enroll;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.queen.biomini.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PrimaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PrimaryFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
    private static final String EMAIL_ADDRESS = "com.queen.biomini.EMAIL_ADDRESS";
    private static final String GENDER = "com.queen.biomini.GENDER";
    private static final String NATIONAL_ID = "com.queen.biomini.PHONE_NUMBER";

    // TODO: Rename and change types of parameters
    private TextInputEditText email;
    private TextInputEditText gender;
    private TextInputEditText nationalId;
    private Button nextButton;
    private Button previousButton;
    private OnNextPrimaryClickListener clickNextListener;
    private OnPreviousPrimaryClickListener clickPreviousListener;

    public PrimaryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p>
     * //     * @param param1 Parameter 1.
     * //     * @param param2 Parameter 2.
     *
     * @return A new instance of fragment PrimaryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PrimaryFragment newInstance(String emailAddress, String gender, String nationalID) {
        PrimaryFragment fragment = new PrimaryFragment();
        Bundle args = new Bundle();

        args.putString(EMAIL_ADDRESS, emailAddress);
        args.putString(GENDER, gender);
        args.putString(NATIONAL_ID, nationalID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_primary, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        email = view.findViewById(R.id.primary_email);
        gender = view.findViewById(R.id.primary_gender);
        nationalId = view.findViewById(R.id.primary_nin);
        nextButton = view.findViewById(R.id.primary_next);
        previousButton = view.findViewById(R.id.primary_previous);

        if (getArguments() != null) {
            email.setText(getArguments().getString(EMAIL_ADDRESS));
            gender.setText(getArguments().getString(GENDER));
            nationalId.setText(getArguments().getString(NATIONAL_ID));
        }

        nextButton.setOnClickListener((View.OnClickListener) this);
        previousButton.setOnClickListener((View.OnClickListener) this);
    }

//    @Override
//    public void onAttach(@NonNull Context context) {
//        super.onAttach(context);
//        clickNextListener = (onNextPrimaryClickListener) context;
//        clickPreviousListener = (onPreviousPrimaryClickListenern) context;
//    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //@Override
    public void onClick(View v) {
        if (v.getId() == R.id.primary_next) {
            clickNextListener.onNextPrimaryClick(
                    email.getText().toString(),
                    gender.getText().toString(),
                    nationalId.getText().toString()
            );
        }

        if (v.getId() == R.id.primary_previous) {
            clickPreviousListener.onPreviousPrimaryClick(
                    email.getText().toString(),
                    gender.getText().toString(),
                    nationalId.getText().toString()
            );
        }

    }

    public interface OnNextPrimaryClickListener {
        void onNextPrimaryClick(String email, String gender, String nationId);
    }

    public interface OnPreviousPrimaryClickListener {
        void onPreviousPrimaryClick(String email, String gender, String nationId);
    }
}