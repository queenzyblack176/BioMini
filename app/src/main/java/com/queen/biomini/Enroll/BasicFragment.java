package com.queen.biomini.Enroll;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.google.android.material.textfield.TextInputEditText;


import com.queen.biomini.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BasicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BasicFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
    private static final String FIRST_NAME = "com.queen.biomini.FIRST_NAME";
    private static final String LAST_NAME = "com.queen.biomini.LAST_NAME";
    private static final String PHONE_NUMBER = "com.queen.biomini.PHONE_NUMBER";

    OnNextBasicClick onNextBasicClick;

    private TextInputEditText firstName;

    private TextInputEditText lastName;

    private TextInputEditText phoneNumber;
    private Button buttonNext;
    private Button buttonPrevious;

    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;

    public BasicFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment BasicFragment.
     */
    // TODO: Rename and change types and number of parameters

    public static BasicFragment newInstance(String firstName, String lastName, String phoneNumber) {
        BasicFragment fragment = new BasicFragment();
        Bundle args = new Bundle();
        args.putString(FIRST_NAME, firstName);
        args.putString(LAST_NAME, lastName);
        args.putString(PHONE_NUMBER, phoneNumber);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_basic, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onNextBasicClick = (OnNextBasicClick) context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        firstName = view.findViewById(R.id.basic_firstName);
        lastName = view.findViewById(R.id.basic_lastName);
        phoneNumber = view.findViewById(R.id.basic_telephone);
        buttonNext = view.findViewById(R.id.basic_next);

        buttonNext.setOnClickListener(this::onClick);
        if (getArguments() != null) {
            firstName.setText(getArguments().getString(FIRST_NAME));
            lastName.setText(getArguments().getString(LAST_NAME));
            phoneNumber.setText(getArguments().getString(PHONE_NUMBER));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //@Override
    public void onClick(View v) {
        if (v.getId() == R.id.basic_next) {
            onNextBasicClick.clickNextBasic(
                    firstName.getText().toString(),
                    lastName.getText().toString(),
                    phoneNumber.getText().toString()
            );
        }
    }

    public interface OnNextBasicClick {
        void clickNextBasic(String firstName, String lastName, String phoneNumber);
    }

}