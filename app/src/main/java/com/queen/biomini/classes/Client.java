package com.queen.biomini.classes;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

public class Client {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name =  "nationalID")
    private String nationalID;

    @ColumnInfo(name =  "firstName")
    private String firstName;

    @ColumnInfo(name =  "lastName")
    private String lastName;

    @ColumnInfo(name =  "telephone")
    private String telephone;

    @ColumnInfo(name =  "email")
    private String email;

    @ColumnInfo(name =  "gender")
    private String gender;

    @ColumnInfo(name =  "tenant")
    private String tenant;

    @ColumnInfo(name =  "thumbImage", typeAffinity = ColumnInfo.BLOB)
    private byte[] thumbImage;

    @ColumnInfo(name =  "fingerPrint", typeAffinity = ColumnInfo.BLOB)
    private byte[] fingerPrint;

    @ColumnInfo(name =  "enrolled")
    private String enrolled;

    @ColumnInfo(name =  "updated")
    private String updated;

    public Client(@NonNull String nationalID, String firstName, String lastName, String telephone, String email, String gender, byte[] thumbImage, byte[] fingerPrint, String enrolled, String updated) {
        this.nationalID = nationalID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephone = telephone;
        this.email = email;
        this.gender = gender;
        this.thumbImage = thumbImage;
        this.fingerPrint = fingerPrint;
        this.enrolled = enrolled;
        this.updated = updated;
    }

    @NonNull
    public String getNationalID() {
        return nationalID;
    }

    public String getEnrolled() {
        return enrolled;
    }

    public void setEnrolled(String enrolled) {
        this.enrolled = enrolled;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public void setNationalID(@NonNull String nationalID) {
        this.nationalID = nationalID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public byte[] getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(byte[] fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public byte[] getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(byte[] thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getFirstName() {
        return firstName;
    }
}
