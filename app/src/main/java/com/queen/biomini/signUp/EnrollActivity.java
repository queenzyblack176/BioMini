package com.queen.biomini.signUp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.queen.biomini.Enroll.BasicFragment;
import com.queen.biomini.Enroll.PrimaryFragment;
import com.queen.biomini.EnrollActivityViewModel;
import com.queen.biomini.R;

public class EnrollActivity extends AppCompatActivity {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String emailAddress;
    private String gender;
    private String nationalID;
    private String tenantName;
    private EnrollActivityViewModel viewModel;
    public byte[] capturedTemplateData;
    private byte[] teacherThumb;

    public static final String CAPTURED_BITMAP = "com.queen.biomini.CAPTURE_BITMAP";
    public static final String ACTION_ENROLL = "com.queen.biomini.ACTION_ENROLL";
    public static final String CAPTURED_TEMPLATE = "com.queen.biomini.CAPTURE_TEMPLATE";
    public static final String FIRST_NAME = "com.queen.biomini.FIRST_NAME";
    public static final String LAST_NAME = "com.queen.biomini.LAST_NAME";
    public static final String PHONE_NUMBER = "com.queen.biomini.PHONE_NUMBER";
    public static final String EMAIL_ADDRESS = "com.queen.biomini.EMAIL_ADDRESS";
    public static final String GENDER = "com.queen.biomini.GENDER";
    public static final String NATIONAL_ID = "com.queen.biomini.NATIONAL_ID";
    public static final String TENANT_NAME = "com.queen.biomini.TENANT_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll);

//        viewModel = new ViewModelProvider(this).get(EnrollActivityViewModel.class);

        if (getSupportFragmentManager().findFragmentById(R.id.enroll_fragment) == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.enroll_fragment, BasicFragment.newInstance(firstName, lastName, phoneNumber))
                    .commit();
        }
        if (getIntent() != null) {
            capturedTemplateData = getIntent().getByteArrayExtra(CAPTURED_BITMAP);
            teacherThumb = getIntent().getByteArrayExtra(CAPTURED_BITMAP);
        }
    }

    //@Override
    public void clickNextBasic(String firstName, String lastName, String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;

        getSupportFragmentManager().beginTransaction().replace(
                R.id.enroll_fragment,
                PrimaryFragment.newInstance(emailAddress, gender, nationalID)
        ).commitNow();
    }
}