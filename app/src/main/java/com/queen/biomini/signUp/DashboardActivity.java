package com.queen.biomini.signUp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.queen.biomini.classes.Client;
import com.queen.biomini.ClientViewModel;
import com.queen.biomini.R;
import com.suprema.BioMiniFactory;
import com.suprema.CaptureResponder;
import com.suprema.IBioMiniDevice;
import com.suprema.IUsbEventHandler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


public class DashboardActivity extends AppCompatActivity {
    //USB MANAGER PERMISSION
    public static final boolean mUsbExternalUSBManager = false;
    private static final String ACTION_USB_PERMISSION = "com.android.USB_PERMISSION";

    //CLOCK DIALOG
    private static final String CLOCK_DIALOG_TAG = "com.queen.biomini.EnrollActivity.CLOCK_IN_OUT_DIALOG";
    public static final int ENROLL_TENANT = 10000;
    private static final String TAG = "Bio Log";

    private UsbManager mUsbManager = null;
    private PendingIntent mPermissionIntent = null;

    //IMPORT BIOMINI
    private static BioMiniFactory mBioMiniFactory = null;
    public static final int REQUEST_WRITE_PERMISSION = 786;
    public IBioMiniDevice mCurrentDevice = null;
    private DashboardActivity mainContext;

    private TextView mLog;
    private TextView mStatus;
    private ScrollView mScroll = null;
    private CardView mCard;
    private TextView textDate;
    private TextClock mClock;
    private Button enroll;
    private Button verify;
    private Button clockIn;
    private Button capture;
    private Button clockOut;
    private Button checkIn;
    public IBioMiniDevice.TemplateData clientCapturedTemplate;
    private Bitmap clientImage;
    private ClientViewModel clientViewModel;

    private IBioMiniDevice.CaptureOption mCaptureOptionDefault = new IBioMiniDevice.CaptureOption();
    private CaptureResponder mCaptureResponseDefault = new CaptureResponder() {
        @Override
        public boolean onCaptureEx(final Object context, final Bitmap capturedImage,
                                   final IBioMiniDevice.TemplateData capturedTemplate,
                                   final IBioMiniDevice.FingerState fingerState) {
            log("onCapture : Capture successful!");
            printState(getResources().getText(R.string.capture_single_ok));
            enableButton(enroll);
            enableButton(verify);
            enableButton(clockOut);
            enableButton(clockIn);

            log(((IBioMiniDevice) context).popPerformanceLog());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (capturedImage != null){
                        ImageView iv = (ImageView) findViewById(R.id.finger_image);
                        if (iv != null) {
                            iv.setImageBitmap(capturedImage);
                        }
                    }
                }
            });

            clientCapturedTemplate = capturedTemplate;
            clientImage = capturedImage;
            return true;
        }

        @Override
        public void onCaptureError(Object contest, int errorCode, String error) {
            log("onCaptureError :" + error + "ErrorCode" + errorCode);
            if (errorCode != IBioMiniDevice.ErrorCode.OK.value()){
                printState(getResources().getText(R.string.capture_fail) + "("+error+")");
                disableButton(clockIn);
                disableButton(clockOut);
                disableButton(verify);
                disableButton(enroll);


            }
        }
    };


    //CAPTURE RESPONSE PREVIEW
    private final CaptureResponder mCapResponsePrev = new CaptureResponder() {
        @Override
        public boolean onCaptureEx(final Object context, final Bitmap capturedImage,
                                   final IBioMiniDevice.TemplateData capturedTemplate,
                                   final IBioMiniDevice.FingerState fingerState) {
            Log.d("CaptureResponsePrev", String.format(Locale.ENGLISH, "CaptureTemplate.size (%d), fingerState(%s)",
                    capturedTemplate == null ? 0 : capturedTemplate.data.length, String.valueOf(fingerState.isFingerExist)));
            printState(getResources().getText((R.string.start_capture_ok)));
            byte[] pImage_raw = null;
            if (mCurrentDevice != null && (pImage_raw = mCurrentDevice.getCaptureImageAsRAW_8()) != null) {
                Log.d("CaptureResponsePrev", String.format(Locale.ENGLISH, "pImage (%d) , FP Quality(%d)",
                        pImage_raw.length, mCurrentDevice.getFPQuality(pImage_raw, mCurrentDevice.getImageWidth(), mCurrentDevice.getImageHeight(), 2)));

            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (capturedImage != null) {
                        ImageView iv = (ImageView) findViewById(R.id.finger_image);
                        if (iv != null) {
                            iv.setImageBitmap(capturedImage);
                        }
                    }
                }
            });

            return true;
        }


        @Override
        public void onCaptureError(Object context, int errorCode, String error) {
            log("onCaptureError : " + error);
            log(((IBioMiniDevice)context).popPerformanceLog());
            if( errorCode != IBioMiniDevice.ErrorCode.OK.value())
                printState(getResources().getText(R.string.start_capture_fail));
        }
    };

//Print state being synchronised
    synchronized public void printState(CharSequence str) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.status_view)).setText(str);
            }
        });
    }

    //log() method being synchronized
    synchronized public void log(final String msg)
    {
        Log.d(TAG, msg);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if( mLog == null){
                    mLog = (TextView) findViewById(R.id.log_text );
                }
                if(mLog != null) {
                    mLog.append(msg + "\n");
                    if(mScroll != null) {
                        mScroll.fullScroll(mScroll.FOCUS_DOWN);
                    }else{
                        Log.d("Log " , "ScrollView is null");
                    }
                }
                else {
                    Log.d("", msg);
                }
            }
        });
    }

    synchronized public void printRev(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DashboardActivity.this, msg, Toast.LENGTH_LONG).show();
//                ((TextView) findViewById(R.id.revText)).setText(msg);
            }
        });
    }

    private final BroadcastReceiver mUsbReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            if (mBioMiniFactory == null) return;
                            mBioMiniFactory.addDevice(device);
                            log(String.format(Locale.ENGLISH, "Initialized device count- BioMiniFactory (%d)", mBioMiniFactory.getDeviceCount()));

                        }

                    }else {
                        Log.d( TAG, "permission denied for device" + device);
                    }

                }
            }
        }
    };

    public void checkDevice() {
        if (mUsbManager == null) return;
        log("check device");
        HashMap <String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        Iterator<UsbDevice> deviceIter =deviceList.values().iterator();
        while (deviceIter.hasNext()) {
            UsbDevice _device = deviceIter.next();
            if (_device.getVendorId() == 0x16d1) {
                //Suprema vendor ID
                mUsbManager.requestPermission(_device, mPermissionIntent);
            }else{

            }
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mainContext = this;
        checkIn = findViewById(R.id.list_teachers);
        enroll = findViewById(R.id.enroll);
        verify = findViewById(R.id.verify);
        clockIn = findViewById(R.id.clock_in);
        clockOut = findViewById(R.id.clock_out);
        capture = findViewById(R.id.capture);

        //disable everything
        disableButton(clockIn);
        disableButton(capture);
        disableButton(verify);
        disableButton(enroll);

        mCaptureOptionDefault.frameRate = IBioMiniDevice.FrameRate.SHIGH;
        mCard = findViewById(R.id.card_background);
        mStatus = findViewById(R.id.status_view);
        mClock = findViewById(R.id.textClock);

        //setting clock hour format
        mClock.setFormat24Hour("hh:mm:ss a EE MMM d");
        mClock.animate();

        textDate = findViewById(R.id.date_view);
        textDate.setText(getCurrentDate());

        clientViewModel = new ViewModelProvider(this).get(ClientViewModel.class);

        //setting the enroll button
        enroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((clientCapturedTemplate != null)) {
                    Intent intent = new Intent (DashboardActivity.this, EnrollActivity.class);
                    intent.setAction(EnrollActivity.ACTION_ENROLL);
                    intent.putExtra(EnrollActivity.CAPTURED_BITMAP, clientImage);
                    intent.putExtra(EnrollActivity.CAPTURED_TEMPLATE, clientCapturedTemplate.data);
                    startActivityForResult(intent, ENROLL_TENANT);

                }else {
                    Toast.makeText(DashboardActivity.this, "Please capture your fingerprint", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // setting verify button
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, VerifyActivity.class);
                intent.setAction(EnrollActivity.ACTION_ENROLL);
                startActivity(intent);
            }
        });

        //Setting the clock in
        findViewById(R.id.clock_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clockInClient(v);
            }
        });

        //setting the fingerprint capture
        findViewById(R.id.capture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCard.setCardBackgroundColor(getResources().getColor(R.color.purple_500));
                ((ImageView) findViewById(R.id.finger_image)).setImageDrawable(getDrawable(R.drawable.ic_fingerprint_black_24dp));
                if (mCurrentDevice != null) {
                    mCurrentDevice.captureSingle(mCaptureOptionDefault, mCaptureResponseDefault, true);
                }
            }
        });

        if (mBioMiniFactory != null) {
            mBioMiniFactory.close();
        }

        resartBiomini();

    }

    //restarting the Biomini Device
    private void resartBiomini() {
        if (mBioMiniFactory != null) {
            mBioMiniFactory.close();
        }
        if (mUsbExternalUSBManager) {
            mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
            mBioMiniFactory = new BioMiniFactory(mainContext, mUsbManager) {
                @Override
                public void onDeviceChange(DeviceChangeEvent event, Object dev) {
                       log("--------------------");
                       log("Fingerprint Scanner Changed : " + event + "using external usb-manager");
                       log("---------------------");
                       handleDevChange(event, dev);
                }
            };

            //
            mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION),0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            registerReceiver(mUsbReciever, filter);
            checkDevice();
        }else {
            mBioMiniFactory = new BioMiniFactory(mainContext) {
                @Override
                public void onDeviceChange(DeviceChangeEvent event, Object dev) {
                    log("------------------------");
                    log("Fingerprint Scanner Changed" + event);
                    log("------------------------");
                    handleDevChange(event, dev);
                }
            };
        }
    }

    //
    private void handleDevChange(IUsbEventHandler.DeviceChangeEvent event, Object dev) {
        if (event == IUsbEventHandler.DeviceChangeEvent.DEVICE_ATTACHED && mCurrentDevice == null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int cnt = 0;
                    while (mBioMiniFactory == null && cnt < 20) {
                        SystemClock.sleep(1000);
                        cnt++;
                    }

                    if (mBioMiniFactory != null) {
                        mCurrentDevice = mBioMiniFactory.getDevice(0);
                        printState(getResources().getText(R.string.device_attached));
                        Log.d(TAG, "Hardware attached :" + mCurrentDevice);
                        if (mCurrentDevice != null) {
                            enableButton(capture);
                            mCard.setCardBackgroundColor(getResources().getColor(R.color.purple_500));
                            log("DeviceName :" + mCurrentDevice.getDeviceInfo().deviceName);
                            log(" SN :" + mCurrentDevice.getDeviceInfo().deviceSN);
                            log("SDK version :" + mCurrentDevice.getDeviceInfo().versionSDK);
                        }
                    }
                }
            }).start();
        }else if (mCurrentDevice != null && event == IUsbEventHandler.DeviceChangeEvent.DEVICE_DETACHED &&mCurrentDevice.isEqual(dev)) {
            mCard.setCardBackgroundColor(getResources().getColor(R.color.purple_500));
            disableButton(capture);
            disableButton(verify);
            disableButton(enroll);
            disableButton(clockOut);
            disableButton(clockIn);
            printState(getResources().getText(R.string.device_detached));
            Log.d(TAG, "Fingerprint Scanner removed" + mCurrentDevice);
            mCurrentDevice = null;
        }
    }


    //When scanning is interupted
    @Override
    protected  void onDestroy() {
        if (mBioMiniFactory != null){
            mBioMiniFactory.close();
            mBioMiniFactory = null;
        }
        if (mUsbExternalUSBManager) {
            unregisterReceiver(mUsbReciever);
        }
        super.onDestroy();
    }

    //Requesting permission
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            log("Permission Granted");
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        requestPermission();
        super.onPostCreate(savedInstanceState);
    }

    //setting the arrival time for each client
    private void clockInClient(View v) {
        clientViewModel.getClients().observe(this, new Observer<List<Client>>(){
            @Override
            public void onChanged(List<Client> clients) {
                for (int i=0; i < clients.size(); i++) {
                    Client client = clients.get(i);
                    if (mCurrentDevice != null) {
                        if (mCurrentDevice.verify(clientCapturedTemplate.data, client.getFingerPrint())) {
                            Snackbar.make(
                                    findViewById(android.R.id.content),
                                    "Clocked In" + client.getFirstName() + getCurrentTime(),
                                    Snackbar.LENGTH_LONG).show();
                        }
                        if (i == clients.size()) {
                            Toast.makeText(DashboardActivity.this, "Client Not Found", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    //setting time
    protected String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date time = Calendar.getInstance().getTime();
        return dateFormat.format(time);
    }

    //setting day
    protected String getCurrentDay() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        Date day = Calendar.getInstance().getTime();
        return dateFormat.format(day);
    }

    //Getting the daily dates
    private String getCurrentDate() {
        Date toDay = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
        return simpleDateFormat.format(toDay);
    }

    //Disabling each button
    private void disableButton(Button view) {
        view.setEnabled(true);
        view.setBackgroundColor(getResources().getColor(android.R.color.system_accent3_900));
        view.setTextColor(getResources().getColor(R.color.white));
    }

    //enabling each button
    private void enableButton(Button view) {
        view.setEnabled(true);
        view.setBackgroundColor(getResources().getColor(R.color.teal_200));
        view.setTextColor(getResources().getColor(R.color.white));
    }
}